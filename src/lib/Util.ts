import { IFormControlConfig } from "./FormControl.interfaces";
import _ from "lodash";

export const generateInitialData = (config: IFormControlConfig) => {
  let formData: Record<string, any> = {};

  for (let prop in config) {
    const fieldState = config[prop];
    const hasValue = fieldState.hasOwnProperty('value');

    formData[prop] = {
      value: hasValue ? fieldState.value : ''
    };

    if (fieldState.hasOwnProperty('valid')) {
      formData[prop].valid = fieldState.valid;
    }

    if (fieldState.hasOwnProperty('disabled')) {
      formData[prop].disabled = fieldState.disabled;
    }

    if (fieldState.hasOwnProperty('required')) {
      formData[prop].required = fieldState.required;
    }
  }

  return formData;
};

export const checkForFalsyValues = (value: any): boolean => {
  let invalid: boolean = false;
  switch (typeof value) {
    case 'number':
      invalid = _.isNull(value);
      break;
    case 'string':
      invalid = !value.length;
      break;
    default:
      invalid = _.isEmpty(value) || _.isUndefined(value) || _.isNull(value);
      break;
  }

  return invalid;
};