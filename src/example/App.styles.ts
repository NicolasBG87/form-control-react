import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  align-items: center;
  flex-direction: column;
  margin: 20px;
`;

export const FormField = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  width: 300px;
`;

export const Input = styled.input`
  width: 100%;
  padding: 5px 10px;
  margin: 5px 0;
`;

export const Button = styled.button`
  display: block;
  background: royalblue;
  border: none;
  width: 300px;
  margin: 25px auto;
  padding: 10px 0;
  color: white;
  font-size: 18px;
  font-weight: bold;
  
  &:disabled {
    background: lightgray;
    color: darkgray;
  }
`;

export const ErrorMessage = styled.span`
  display: block;
  height: 18px;
  font-size: 16px;
  color: red;
`;