import React from 'react';
import { useForm } from "../lib";
import { config } from "./App.config";
import { Button, ErrorMessage, Form, FormField, Input } from "./App.styles";

const App: React.FC = () => {
  const {
    getValues,
    getFields,
    handleChange,
    reset,
    valid,
    validate,
  } = useForm(config);

  const handleSubmit = () => {
    const isFormValid = validate();
    isFormValid && console.log('Values: ', getValues());
  };

  const { firstName, lastName, age } = getFields();
  return (
    <>
      <Form>
        <FormField>
          <label htmlFor="firstName">
            First Name
            {firstName.required && <span style={{ color: 'red' }}>*</span>}
          </label>
          <Input
            onChange={handleChange}
            type="text"
            name='firstName'
            {...firstName}
          />
          <ErrorMessage>{firstName.errorMsg}</ErrorMessage>
        </FormField>

        <FormField>
          <label htmlFor="lastName">
            Last Name
            {lastName.required && <span style={{ color: 'red' }}>*</span>}
          </label>
          <Input
            onChange={handleChange}
            type="text"
            name='lastName'
            {...lastName}
          />
          <ErrorMessage>{lastName.errorMsg}</ErrorMessage>
        </FormField>

        <FormField>
          <label htmlFor="age">
            Age
            {age.required && <span style={{ color: 'red' }}>*</span>}
          </label>
          <Input
            onChange={handleChange}
            type="number"
            name='age'
            {...age}
          />
          <ErrorMessage>{age.errorMsg}</ErrorMessage>
        </FormField>
      </Form>
      <Button onClick={reset}>Reset</Button>
      <Button onClick={handleSubmit} disabled={!valid}>Submit</Button>
    </>
  );
};

export default App;
