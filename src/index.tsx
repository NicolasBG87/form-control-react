import React from 'react';
import ReactDOM from 'react-dom';
import App from './example/App';
import 'reset-css';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
`;

const app = (
  <>
    <App />
    <GlobalStyle />
  </>
);

ReactDOM.render(app, document.getElementById('root'));
